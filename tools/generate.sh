#!/usr/bin/env sh

tools/download_latest_podman_swagger.sh

tools/swagger-cli.sh generate \
                      -l java \
                      -i tools/podman_swagger.yaml \
                      -o gen/ \
                      -c tools/swagger_generator.json

chmod +x gradlew
