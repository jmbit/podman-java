package de.jmbit.java.podman;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.time.Duration;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.jr.ob.JSON;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.newsclub.net.unix.AFSocketFactory;
import org.newsclub.net.unix.AFUNIXSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/* Wrapper around the Podman Socket, allowing Communication with the Backend
 *
 *
 *
 *
 *
 *
 */

public class PodmanAPI {
    final Logger logger = LoggerFactory.getLogger(PodmanAPI.class);

    // Hacky way to get the Socket of rootless Podman
    private AFUNIXSocketAddress socketAddress() throws SocketException {
        File rootSocketFile = new File("/run/podman/podman.sock");
        long uid = new com.sun.security.auth.module.UnixSystem().getUid();
        File userSocketFile = new File(String.format("/run/user/%d/podman/podman.sock", uid));
        File socketFile;
        AFUNIXSocketAddress afunixSocketAddress;
        if (!rootSocketFile.canWrite()) {
            socketFile = userSocketFile;
        }
        else {
            socketFile = rootSocketFile;
        }
        try{
            afunixSocketAddress = AFUNIXSocketAddress.of(socketFile);
        } catch (SocketException e) {
            logger.error(String.format("Could not Bind to socket: %s", e));
            afunixSocketAddress = AFUNIXSocketAddress.of(socketFile);
        }
        return afunixSocketAddress;
    }

    private OkHttpClient client() throws SocketException{
        AFUNIXSocketAddress socketAddress = socketAddress();

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .socketFactory(new AFSocketFactory.FixedAddressSocketFactory(socketAddress))
                .callTimeout(Duration.ofSeconds(60));

        return builder.build();
    }
    public int connectionStatus() {
        int status = 418;

        OkHttpClient client;
        try {
            client = client();
        } catch (SocketException e) {
            logger.error(String.format("Could not access Podman Socket, %s", e));
            return 500;
        }

        Request request = new Request.Builder().url("http://d/v4.0.0/libpod/info").build();
        try (Response response = client.newCall(request).execute()) {
            return response.code();
        } catch (IOException | NullPointerException e) {
            logger.error(String.format("Error after trying to access Podman API: %s", e));
            return 500;
        }

    }

}
